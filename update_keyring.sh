#!/bin/bash
src_dir=$1
keychain_fname=$2

if [ ! -d "${src_dir}" ]
then
  echo "first argument must be a directory with .asc key files, e.g. ../../cdssoft-release-bookworm"
  exit 1
fi

if [[ ${keychain_fname: -4} != ".gpg"  ]]
then
  echo "second argument '${keychain_fname}' must be a filename ending in '.gpg'"
  exit 1
fi

if [ ! -d files ]
then
  echo "must be run in the root directory of the cdssoft puppet module"
  exit 1
fi

asc_files=$( ls $src_dir/*.asc )

if [ -z "${asc_files}" ]
then
  echo "no keys to add in ${src_dir}"
  exit 1
fi

target_dir=files/etc/apt/keys
if [ ! -e $target_dir ]
then
  echo "${target_dir} doesn't exist. Creating."
  mkdir -p $target_dir
fi

if [ ! -d $target_dir ]
then
  echo "${target_dir} must be a directory, but isn't"
  exit 1
fi

keychain_file="${target_dir}/${keychain_fname}"

if [ -e $keychain_file ]
then
  echo "${keychain_file} already exists.  It will be replaced"
fi

mkdir -p files/etc/apt/keys


echo "processing key files in ${src_dir}"

tmp_file="/tmp/cdssoft_key_chain.gpg"
if [ -f $tmp_file ]
then
  rm $tmp_file
fi

for key_file in $asc_files
do
  echo "adding ${key_file}"
  gpg --dearmor < $key_file >> $tmp_file
  if ! $!
  then
    echo "Failed to add ${key_file}"
  fi
done

cp $tmp_file $keychain_file
if $!
then
  echo "created ${keychain_file}"
else
  echo "failed to copy ${keychain_file}"
  exit 1
fi
