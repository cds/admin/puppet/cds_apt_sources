class cds_apt_sources::cdssoft (
  Enum[production, testing, unstable] $stability = production
){

  include ::cds_apt_sources::key_directory
  include ::cds_apt_sources::apt_update

  # make sure stretch and older also get lscsoft
  $os_release_num = Integer($facts['os']['release']['major'])
  if $os_release_num < 10 {
    include ::cds_apt_sources::lscsoft
  }

  $codename = $facts['os']['distro']['codename']

  case $stability {
    'production': {
      $suite_extension = ''
    }
    'testing': {
      $suite_extension = '-testing'
    }
    'unstable': {
      $suite_extension = '-unstable'
    }
    default: {
      fail('stability attribute MUST BE SET to production, testing, or unstable')
    }
  }

  # keyring
  $keyring_file = "${::cds_apt_sources::key_directory::key_directory}/cdssoft.gpg"
  file {$keyring_file:
    owner   => 'root',
    group   => 'root',
    mode    => '0444',
    source => "puppet:///modules/cds_apt_sources/etc/apt/keys/cdssoft.gpg",
    notify => Exec['cds_apt_sources apt update']
  }

  # source
  $source_file = '/etc/apt/sources.list.d/cdssoft.sources'
  file { $source_file:
    owner   => 'root',
    group   => 'root',
    mode    => '0444',
    notify => Exec['cds_apt_sources apt update'],
    content => @("EOF"/L)
    Types: deb
    URIs: https://apt.ligo-wa.caltech.edu/debian
    Suites: ${codename}${suite_extension}
    Components: main
    Signed-By: ${keyring_file}
    | EOF
  }

  $old_packages = ["cdssoft-release-${codename}", "cdssoft-testing-${codename}",]

  # priority pinning
  file { '/etc/apt/preferences.d/cdssoft-pinning':
    owner   => 'root',
    group   => 'root',
    mode    => '0444',
    notify => Exec['cds_apt_sources apt update'],
    require => $old_packages.map|$package|{Package[$package]},
    content => @("EOF"/L)
      # Puppet managed file, do not modify
      Package: *
      Pin: release o=LIGO,n=${codename}${suite_extension},l=LIGO CDS software
      Pin-Priority: 900
      | EOF
  }

  # cleanup from other ways of installing cdssoft
  file {'/etc/apt/sources.list.d/cdssoft.list': ensure => absent, }
  package {$old_packages:
    ensure => purged
  }

}
