class cds_apt_sources::apt_update {
  exec {'cds_apt_sources apt update':
    command => '/usr/bin/apt-get update',
    refreshonly => true,
  }
}