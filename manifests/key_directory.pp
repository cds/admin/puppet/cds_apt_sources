class cds_apt_sources::key_directory {
  $key_directory = '/etc/apt/keyrings'
  file {$key_directory: ensure => directory, }
}