class cds_apt_sources::cdssoft_restricted (
){
  include ::cds_apt_sources::key_directory
  include ::cds_apt_sources::apt_update

  $codename = $facts['os']['distro']['codename']

  # keyring
  $keyring_file = "${::cds_apt_sources::key_directory::key_directory}/cds_jenkins.gpg"
  file {$keyring_file:
    owner   => 'root',
    group   => 'root',
    mode    => '0444',
    source => "puppet:///modules/cds_apt_sources/etc/apt/keys/cds_jenkins.gpg",
    notify => Exec['cds_apt_sources apt update']
  }

  # source
  $restricted_credentials=lookup('cds::restricted::credentials')

  $source_file = '/etc/apt/sources.list.d/cds-restricted.sources'
  file { $source_file:
    owner   => 'root',
    group   => 'root',
    mode    => '0444',
    notify => Exec['cds_apt_sources apt update'],
    content => @("EOF"/L)
    Types: deb
    URIs: https://${restricted_credentials}@apt.ligo-wa.caltech.edu/debian
    Suites: ${codename}-restricted
    Components: main
    Signed-By: ${keyring_file}
    | EOF
  }

  # cleanup from other ways of installing cdssoft
  file {'/etc/apt/sources.list.d/cds-restriced.list': ensure => absent, }


}
