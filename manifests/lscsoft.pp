class cds_apt_sources::lscsoft (
){

  include ::cds_apt_sources::key_directory
  include ::cds_apt_sources::apt_update

  $codename = $facts['os']['distro']['codename']

  # keyring
  $keyring_file = "${::cds_apt_sources::key_directory::key_directory}/lscsoft.gpg"
  file {$keyring_file:
    owner   => 'root',
    group   => 'root',
    mode    => '0444',
    source => "puppet:///modules/cds_apt_sources/etc/apt/keys/lscsoft.gpg",
    notify => Exec['cds_apt_sources apt update']
  }

  # source
  $source_file = '/etc/apt/sources.list.d/lscsoft.sources'
  file { $source_file:
    owner   => 'root',
    group   => 'root',
    mode    => '0444',
    notify => Exec['cds_apt_sources apt update'],
    content => @("EOF"/L)
    Types: deb
    URIs: http://software.ligo.org/gridtools/debian/
    Suites: ${codename}
    Components: main
    Signed-By: ${keyring_file}

    Types: deb
    URIs: https://hypatia.aei.mpg.de/lsc-amd64-${codename}/
    Suites: /
    Trusted: yes
    | EOF
  }

  # cleanup from other ways of installing cdssoft
  file {'/etc/apt/sources.list.d/lscsoft.list': ensure => absent, }


}
